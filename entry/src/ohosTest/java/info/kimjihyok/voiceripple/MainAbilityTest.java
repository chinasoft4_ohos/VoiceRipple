/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.kimjihyok.voiceripple;

import info.kimjihyok.ripplelibrary.util.ColorConverUtils;
import info.kimjihyok.ripplelibrary.util.Constant;
import info.kimjihyok.ripplelibrary.util.ResUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * 单元测试
 *
 * @author VoiceRipple
 * @since 2021-05-26
 */
public class MainAbilityTest {
    Ability ability = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();

    @Test
    public void onStart() {
        int[] rgb = ColorConverUtils.colorToRgb(Constant.NUMBER65536);
        assertEquals(rgb[1],0);
        assertEquals(rgb[Constant.NUMBER2],0);
        String str = ResUtil.getString(ability.getApplicationContext(),1);
        assertEquals(str,"");
    }
}