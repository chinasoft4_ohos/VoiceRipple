/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.kimjihyok.voiceripple;

import ohos.aafwk.ability.AbilityPackage;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.File;

/**
 * MyApplication类
 *
 * @since 2021-04-30
 */
public class MyApplication extends AbilityPackage {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD000f00, "dqh");

    /**
     * onInitialize
     */
    @Override
    public void onInitialize() {
        super.onInitialize();
        File file = new File(getContext().getFilesDir(), "record.mp3");
        if (file.exists()) {
            boolean isDelete = file.delete();
            HiLog.info(LABEL_LOG, isDelete + "");
        }
    }
}
