/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.kimjihyok.ripplelibrary.util;

/**
 * 颜色转换工具类
 *
 * @since 2021-04-16
 */
public final class ColorConverUtils {
    private static final int NUMBER8 = 8;
    private static final int NUMBER16 = 16;

    private ColorConverUtils() {
    }

    /**
     * 颜色id转rgb
     *
     * @param color color
     * @return 颜色id转rgb
     */
    public static int[] colorToRgb(int color) {
        // color id 转RGB
        int red = (color & Constant.DEFAULT) >> NUMBER16;
        int green = (color & Constant.DEFAULT2) >> NUMBER8;
        int blue = color & Constant.DEFAULT3;
        return new int[]{red, green, blue};
    }
}
