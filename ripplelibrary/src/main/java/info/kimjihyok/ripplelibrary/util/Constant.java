/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.kimjihyok.ripplelibrary.util;

/**
 * 常量工具类
 *
 * @since 2021-04-16
 */
public final class Constant {
    /**
     * number 1.4
     */
    public static final float NUMBER1_4 = 1.4f;
    /**
     * numner 0.5f
     */
    public static final float ZOREFIVEF = 0.5f;
    /**
     * number 1.1f
     */
    public static final float NUMBER11F = 1.1f;
    /**
     * number 20.0f
     */
    public static final float NUMBER20F = 20.0f;
    /**
     * number 8
     */
    public static final int NUMBER8 = 8;
    /**
     * number 2
     */
    public static final int NUMBER2 = 2;
    /**
     * number 5
     */
    public static final int NUMBER5 = 5;
    /**
     * NUMBER6
     */
    public static final int NUMBER6 = 6;
    /**
     * number 3
     */
    public static final int NUMBER3 = 3;
    /**
     * number 9
     */
    public static final int NUMBER9 = 9;
    /**
     * number 10
     */
    public static final int NUMBER10 = 10;
    /**
     * number 14
     */
    public static final int NUMBER14 = 14;
    /**
     * number 20
     */
    public static final int NUMBER20 = 20;
    /**
     * number 24
     */
    public static final int NUMBER24 = 24;
    /**
     * number 33
     */
    public static final int NUMBER33 = 33;
    /**
     * number 42
     */
    public static final int NUMBER42 = 42;
    /**
     * number 36
     */
    public static final int NUMBER36 = 36;
    /**
     * number 41
     */
    public static final int NUMBER41 = 41;
    /**
     * number 50
     */
    public static final int NUMBER50 = 50;
    /**
     * number 54
     */
    public static final int NUMBER54 = 54;
    /**
     * number 60
     */
    public static final int NUMBER60 = 60;
    /**
     * number 72
     */
    public static final int NUMBER72 = 72;
    /**
     * number 90
     */
    public static final int NUMBER90 = 90;
    /**
     * number 100
     */
    public static final int NUMBER100 = 100;
    /**
     * number 124
     */
    public static final int NUMBER124 = 124;
    /**
     * number 165
     */
    public static final int NUMBERF165 = 165;
    /**
     * number 180
     */
    public static final int NUMBER180 = 180;
    /**
     * number -90
     */
    public static final int NUMBERF90 = -90;
    /**
     * number 360
     */
    public static final int NUMBER360 = 360;
    /**
     * number 360.0f
     */
    public static final float NUMBER360F = 360.0f;
    /**
     * number 204
     */
    public static final int NUMBER204 = 204;
    /**
     * number 238
     */
    public static final int NUMBER238 = 238;
    /**
     * number 255
     */
    public static final int NUMBER255 = 255;
    /**
     * number 270
     */
    public static final int NUMBER270 = 270;
    /**
     * number 1003
     */
    public static final int NUMBER1003 = 1003;
    /**
     * number 100.0
     */
    public static final float NUMBER100F = 100.0f;
    /**
     * number 0.1f
     */
    public static final float NUMBER01F = 0.1f;
    /**
     * number 0.2f
     */
    public static final float NUMBER02F = 0.2f;
    /**
     * number 0.3f
     */
    public static final float NUMBER03F = 0.3f;
    /**
     * number 0.4f
     */
    public static final float NUMBER04F = 0.4f;
    /**
     * number 0.5f
     */
    public static final float NUMBER05F = 0.5f;
    /**
     * number 0.6f
     */
    public static final float NUMBER06F = 0.6f;
    /**
     * number 0.7f
     */
    public static final float NUMBER07F = 0.7f;
    /**
     * number 0.8f
     */
    public static final float NUMBER08F = 0.8f;
    /**
     * number 0.9f
     */
    public static final float NUMBER09F = 0.9f;
    /**
     * number 65536
     */
    public static final int NUMBER65536 = -65536;
    /**
     * number 10000
     */
    public static final float NUMBER10000F = 10000.0f;
    /**
     * DEFAULT
     */
    public static final int DEFAULT = 0xff0000;
    /**
     * DEFAULT2
     */
    public static final int DEFAULT2 = 0x00ff00;
    /**
     * DEFAULT3
     */
    public static final int DEFAULT3 = 0x0000ff;
    /**
     * COLOR00FFFFFF
     */
    public static final int COLOR00FFFFFF = 0x00FFFFFF;
    /**
     * COLOR40000000
     */
    public static final int COLOR40000000 = 0x40000000;
    /**
     * 0x50
     */
    public static final int COLOR0X50 = 0x50;

    private Constant() {
    }
}
