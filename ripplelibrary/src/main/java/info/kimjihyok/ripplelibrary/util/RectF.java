/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.kimjihyok.ripplelibrary.util;

import ohos.agp.utils.RectFloat;

/**
 * 文RectF
 *
 * @since 2021-04-26
 */
public class RectF extends RectFloat {
    /**
     * 构造函数
     */
    public RectF() {
        super();
    }

    /**
     * 构造函数
     *
     * @param left left
     * @param top top
     * @param right right
     * @param bottom bottom
     */
    public RectF(float left, float top, float right, float bottom) {
        super(left, top, right, bottom);
    }
}