package info.kimjihyok.ripplelibrary.listener;

/**
 * Created by jihyokkim on 2017. 8. 28..
 */
public interface RecordingListener {
    /**
     * 录制停止
     */
    void onRecordingStopped();

    /**
     * 录制开始
     */
    void onRecordingStarted();
}
