package info.kimjihyok.ripplelibrary.renderer;

import info.kimjihyok.ripplelibrary.util.Constant;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * Created by jkimab on 2017. 9. 1..
 */
public class CircleRippleRenderer extends Renderer {
    private final Paint buttonPaint;
    private final Paint ripplePaint;
    private final Paint rippleBackgroundPaint;

    /**
     * 构造函数
     *
     * @param ripplePaint ripplePaint
     * @param rippleBackgroundPaint rippleBackgroundPaint
     * @param buttonPaint buttonPaint
     */
    public CircleRippleRenderer(Paint ripplePaint, Paint rippleBackgroundPaint, Paint buttonPaint) {
        this.ripplePaint = ripplePaint;
        this.rippleBackgroundPaint = rippleBackgroundPaint;
        this.buttonPaint = buttonPaint;
    }

    /**
     * 绘制函数
     *
     * @param canvas canvas
     * @param xx xx
     * @param yy yy
     * @param buttonRadius buttonRadius
     * @param rippleRadius rippleRadius
     * @param rippleBackgroundRadius rippleBackgroundRadius
     */
    @Override
    public void render(Canvas canvas, int xx, int yy, int buttonRadius, int rippleRadius, int rippleBackgroundRadius) {
        super.render(canvas, xx, yy, buttonRadius, rippleRadius, rippleBackgroundRadius);
        canvas.drawCircle(xx, yy, rippleRadius, ripplePaint);
        canvas.drawCircle(xx, yy, rippleBackgroundRadius, rippleBackgroundPaint);
        canvas.drawCircle(xx, yy, buttonRadius, buttonPaint);
    }

    @Override
    public void changeColor(int color) {
        ripplePaint.setColor(new Color(color));
        rippleBackgroundPaint.setColor(new Color((color & Constant.COLOR00FFFFFF) | Constant.COLOR40000000));
    }
}
