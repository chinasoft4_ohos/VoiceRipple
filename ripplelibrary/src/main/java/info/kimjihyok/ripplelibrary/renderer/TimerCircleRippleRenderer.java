package info.kimjihyok.ripplelibrary.renderer;

import info.kimjihyok.ripplelibrary.util.Constant;
import info.kimjihyok.ripplelibrary.util.RectF;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * Created by jkimab on 2017. 8. 30..
 */
public class TimerCircleRippleRenderer extends CircleRippleRenderer {
    private RectF rect;
    private int strokeWidth;
    private final Paint timerPaint;
    private Paint timerBackgroundPaint;
    private final double maxTimeMilliseconds;
    private double currentTimeMilliseconds;
    private TimerRendererListener listener;

    public interface TimerRendererListener {
        /**
         * 停止录制
         */
        void stopRecording();

        /**
         * 开始录制
         */
        void startRecording();
    }

    /**
     * 构造函数
     *
     * @param ripplePaint ripplePaint
     * @param rippleBackgroundPaint rippleBackgroundPaint
     * @param buttonPaint buttonPaint
     * @param timerPaint timerPaint
     * @param maxTimeMilliseconds maxTimeMilliseconds
     * @param currentTimeMilliseconds currentTimeMilliseconds
     */
    public TimerCircleRippleRenderer(Paint ripplePaint, Paint rippleBackgroundPaint, Paint buttonPaint,
                                     Paint timerPaint, double maxTimeMilliseconds, double currentTimeMilliseconds) {
        super(ripplePaint, rippleBackgroundPaint, buttonPaint);
        this.timerPaint = timerPaint;
        this.maxTimeMilliseconds = maxTimeMilliseconds;
        this.currentTimeMilliseconds = currentTimeMilliseconds;
        init();
    }

    public void setTimerRendererListener(TimerRendererListener timeListener) {
        this.listener = timeListener;
    }

    private void init() {
        rect = new RectF();
        timerBackgroundPaint = new Paint();
        timerBackgroundPaint.setColor(new Color(Color.getIntColor("#EEEEEE")));
        timerBackgroundPaint.setStrokeWidth(Constant.NUMBER20);
        timerBackgroundPaint.setAntiAlias(true);
        timerBackgroundPaint.setStrokeCap(Paint.StrokeCap.SQUARE_CAP);
        timerBackgroundPaint.setStyle(Paint.Style.STROKE_STYLE);
    }

    @Override
    public void render(Canvas canvas, int xx, int yy, int buttonRadius, int rippleRadius, int rippleBackgroundRadius) {
        super.render(canvas, xx, yy, buttonRadius, rippleRadius, rippleBackgroundRadius);
        rect = new RectF(xx - buttonRadius + strokeWidth / Constant.NUMBER2, yy - buttonRadius + strokeWidth
                / Constant.NUMBER2,xx + buttonRadius - strokeWidth / Constant.NUMBER2,
                yy + buttonRadius - strokeWidth / Constant.NUMBER2);
        canvas.drawArc(rect, new Arc(Constant.NUMBERF90, Constant.NUMBER360, false), timerBackgroundPaint);
        canvas.drawArc(rect, new Arc(Constant.NUMBERF90, (float) (Constant.NUMBER360F
                * (currentTimeMilliseconds / maxTimeMilliseconds)), false), timerPaint);

        if (currentTimeMilliseconds >= maxTimeMilliseconds) {
            listener.stopRecording();
        }
    }

    @Override
    public void changeColor(int color) {
        super.changeColor(color);
    }

    public void setCurrentTimeMilliseconds(int currentTimeMilliseconds) {
        this.currentTimeMilliseconds = currentTimeMilliseconds;
    }

    /**
     * 设置二级进度位置
     *
     * @param strokeWidth strokeWidth
     * @throws IllegalArgumentException
     */
    public void setStrokeWidth(int strokeWidth) {
        if (strokeWidth % Constant.NUMBER2 != 0) {
            throw new IllegalArgumentException("Stroke Width should be an even number!");
        }

        this.strokeWidth = strokeWidth;
        timerPaint.setStrokeWidth(strokeWidth);
    }
}
