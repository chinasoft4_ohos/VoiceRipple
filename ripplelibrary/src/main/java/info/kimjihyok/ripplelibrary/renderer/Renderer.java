package info.kimjihyok.ripplelibrary.renderer;

import ohos.agp.render.Canvas;

/**
 * Created by jkimab on 2017. 8. 30..
 */
public abstract class Renderer {
    /**
     * 绘制
     *
     * @param canvas canvas
     * @param xx xx
     * @param yy yy
     * @param buttonRadius buttonRadius
     * @param rippleRadius rippleRadius
     * @param rippleBackgroundRadius rippleBackgroundRadius
     */
    public void render(Canvas canvas, int xx, int yy, int buttonRadius, int rippleRadius, int rippleBackgroundRadius) {
    }

    /**
     * 改变颜色
     *
     * @param color color
     */
    public abstract void changeColor(int color);
}

