# VoiceRipple

#### 项目介绍

- 项目名称：VoiceRipple
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个超级棒的音波语音录制按钮，可以跟随用户的语音大小产生波状动态效果，可以通过转动颜色转盘来更换波纹颜色，可以对语音进行 采样率和衰减率的调整，录音后进行音乐播放。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支

#### 效果演示

<img src="gif/voiceripple.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:voiceripple:1.0.0')
   ......
}
```
在sdk6，DevEco Studio2.2 Beta1 下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

Demo中调用ripplelibrary组件库，操作展示组件功能。

1、布局文件中引用界面效果：
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/apk/res-auto"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">

    <DependentLayout
        ohos:height="180vp"
        ohos:width="match_parent"
        ohos:alignment="center"
        ohos:background_element="#E9E9E9">

        <info.kimjihyok.ripplelibrary.VoiceRippleView
            ohos:id="$+id:voiceRipple"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:alignment="center"
            />

        <Image
            ohos:id="$+id:videoimage"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:scale_mode="center"
            ohos:image_src="$media:record"
            ohos:vertical_center="true"
            ohos:horizontal_center="true"
            />

    </DependentLayout>

    <ScrollView
        ohos:height="match_parent"
        ohos:width="match_parent"
        >

        <DirectionalLayout
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:orientation="vertical"
            >

            <Text
                ohos:height="match_content"
                ohos:width="match_parent"
                ohos:text="Control Panel"
                ohos:text_alignment="center"
                ohos:text_size="18fp"
                ohos:top_margin="10vp"/>

            <info.kimjihyok.ripplelibrary.ColorPicker
                ohos:id="$+id:colorpicker"
                ohos:height="345vp"
                ohos:width="match_content"
                ohos:top_margin="-40vp"
                />

            <DirectionalLayout
                ohos:height="match_parent"
                ohos:width="match_parent"
                ohos:orientation="vertical"
                ohos:padding="20vp">

                <DirectionalLayout
                    ohos:height="match_content"
                    ohos:width="match_parent"
                    ohos:orientation="horizontal">

                    <Text
                        ohos:height="match_content"
                        ohos:width="match_content"
                        ohos:text="Icon Size: "
                        ohos:text_alignment="center"
                        ohos:text_size="13fp"/>

                    <Slider
                        ohos:id="$+id:iconsize"
                        ohos:height="match_content"
                        ohos:width="match_parent"
                        ohos:background_instruct_element="#DEDADA"
                        ohos:progress="10"
                        ohos:progress_color="#FFFF0000"
                        ohos:scrollbar_color="#FFFF0000"
                        />

                </DirectionalLayout>

                <DirectionalLayout
                    ohos:height="match_content"
                    ohos:width="match_parent"
                    ohos:orientation="horizontal"
                    ohos:top_margin="20vp">

                    <Text
                        ohos:height="match_content"
                        ohos:width="match_content"
                        ohos:text="Ripple Size: "
                        ohos:text_alignment="center"
                        ohos:text_size="13fp"/>

                    <Slider
                        ohos:id="$+id:ripplesize"
                        ohos:height="match_content"
                        ohos:width="match_parent"
                        ohos:background_instruct_element="#DEDADA"
                        ohos:progress="50"
                        ohos:progress_color="#FFFF0000"
                        ohos:scrollbar_color="#FFFF0000"
                        />

                </DirectionalLayout>

                <DirectionalLayout
                    ohos:height="match_content"
                    ohos:width="match_parent"
                    ohos:orientation="vertical">

                    <RadioContainer
                        ohos:id="$+id:decayradio_container"
                        ohos:height="match_content"
                        ohos:width="match_content"
                        ohos:layout_alignment="left"
                        ohos:orientation="horizontal"
                        ohos:top_margin="16vp">

                        <Text
                            ohos:height="match_content"
                            ohos:width="match_content"
                            ohos:text="Decay Rate: "
                            ohos:text_alignment="center"
                            ohos:text_size="13fp"/>


                        <RadioButton
                            ohos:id="$+id:decayradio_low"
                            ohos:height="40vp"
                            ohos:width="match_content"
                            ohos:check_element="$media:radionormal"
                            ohos:marked="true"
                            ohos:text="LOW"
                            ohos:text_color_on="#000000"
                            ohos:text_size="13fp"/>

                        <RadioButton
                            ohos:id="$+id:decayradio_med"
                            ohos:height="40vp"
                            ohos:width="match_content"
                            ohos:check_element="$media:radionormal"
                            ohos:text="MED"
                            ohos:text_color_on="#000000"
                            ohos:text_size="13fp"/>

                        <RadioButton
                            ohos:id="$+id:decayradio_hight"
                            ohos:height="40vp"
                            ohos:width="match_content"
                            ohos:check_element="$media:radionormal"
                            ohos:text="HIGH"
                            ohos:text_color_on="#000000"
                            ohos:text_size="13fp"/>
                    </RadioContainer>

                    <RadioContainer
                        ohos:id="$+id:sampleradio_container"
                        ohos:height="match_content"
                        ohos:width="match_content"
                        ohos:layout_alignment="left"
                        ohos:orientation="horizontal"
                        ohos:top_margin="10vp"
                        >

                        <Text
                            ohos:height="match_content"
                            ohos:width="match_content"
                            ohos:text="Sample Rate: "
                            ohos:text_alignment="center"
                            ohos:text_size="13fp"/>


                        <RadioButton
                            ohos:id="$+id:sampleradio_low"
                            ohos:height="40vp"
                            ohos:width="match_content"
                            ohos:check_element="$media:radionormal"
                            ohos:marked="true"
                            ohos:text="LOW"
                            ohos:text_color_on="#000000"
                            ohos:text_size="13fp"/>

                        <RadioButton
                            ohos:id="$+id:sampleradio_med"
                            ohos:height="40vp"
                            ohos:width="match_content"
                            ohos:check_element="$media:radionormal"
                            ohos:text="MED"
                            ohos:text_color_on="#000000"
                            ohos:text_size="13fp"/>

                        <RadioButton
                            ohos:id="$+id:sampleradio_high"
                            ohos:height="40vp"
                            ohos:width="match_content"
                            ohos:check_element="$media:radionormal"
                            ohos:text="HIGH"
                            ohos:text_color_on="#000000"
                            ohos:text_size="13fp"/>
                    </RadioContainer>

                </DirectionalLayout>

                <DirectionalLayout
                    ohos:height="match_content"
                    ohos:width="match_parent"
                    ohos:orientation="vertical">

                    <Button
                        ohos:id="$+id:playervideo"
                        ohos:height="match_content"
                        ohos:width="match_content"
                        ohos:background_element="#DEDADA"
                        ohos:bottom_padding="5vp"
                        ohos:layout_alignment="center"
                        ohos:left_padding="15vp"
                        ohos:right_padding="15vp"
                        ohos:text="PLAY"
                        ohos:text_size="15fp"
                        ohos:top_margin="10vp"
                        ohos:top_padding="5vp"
                        />

                </DirectionalLayout>
            </DirectionalLayout>

        </DirectionalLayout>

    </ScrollView>

</DirectionalLayout>
```

其中 自定义颜色选择器 是ColorPicker类：

```xml
<info.kimjihyok.ripplelibrary.ColorPicker
    ohos:id="$+id:colorpicker"
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:top_margin="-40vp"
    />
```

自定义振幅波纹效果 是VoiceRippleView类。

```xml
<info.kimjihyok.ripplelibrary.VoiceRippleView
    ohos:id="$+id:voiceRipple"
    ohos:height="match_content"
    ohos:width="match_content"
    ohos:alignment="center"
    />
```
2、在自定义中 振幅波形图实现
```java
    @Override
    public void render(Canvas canvas, int xx, int yy, int buttonRadius, int rippleRadius, int rippleBackgroundRadius) {
        super.render(canvas, xx, yy, buttonRadius, rippleRadius, rippleBackgroundRadius);
        rect = new RectF(xx - buttonRadius, yy - buttonRadius, xx + buttonRadius,
                yy + buttonRadius);
        canvas.drawArc(rect, new Arc(Constant.NUMBERF90, Constant.NUMBER360, false), timerBackgroundPaint);
        canvas.drawArc(rect, new Arc(Constant.NUMBERF90, (float) (Constant.NUMBER360F
                * (currentTimeMilliseconds / maxTimeMilliseconds)), false), timerPaint);

        if (currentTimeMilliseconds >= maxTimeMilliseconds) {
            listener.stopRecording();
        }
    }

    @Override
    public void render(Canvas canvas, int x, int y, int buttonRadius, int rippleRadius, int rippleBackgroundRadius) {
        super.render(canvas, x, y, buttonRadius, rippleRadius, rippleBackgroundRadius);
        canvas.drawCircle(x, y, rippleRadius, ripplePaint);
        canvas.drawCircle(x, y, rippleBackgroundRadius, rippleBackgroundPaint);
        canvas.drawCircle(x, y, buttonRadius, buttonPaint);
    }

```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

当前版本demo部分功能暂不支持模拟器运行

#### 版本迭代

- 1.0.0

#### 版权和许可信息

Apache License 2.0